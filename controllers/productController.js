const Product = require("../models/productModel");

const getAllProducts = async (req, res) => {
    const products = await Product.find({});
    res.status(200).json(products)
}

const addNewProduct = async (req, res) => {
    const product = new Product(req.body)
    await product.save();
    res.status(201).json(product)
}

const getProductById = async (req, res) => {
    const product = await Product.findById(req.params.productId).exec();
    res.status(200).json(product)
}

const updateProduct = async (req, res) => {
    const updatedProduct = Product.findByIdAndUpdate(req.params.productId, req.body, { new: true })
    res.status(200).json(updatedProduct)
}

const deleteProduct = async (req, res) => {
    await Product.findByIdAndDelete(req.params.productId)
    res.send('Deleted')
}

module.exports = {
    getAllProducts,
    addNewProduct,
    getProductById,
    updateProduct,
    deleteProduct
}