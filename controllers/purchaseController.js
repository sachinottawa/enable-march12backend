const purchaseItem = (req, res, next) => {
    next()
}

const sendMail = (req, res) => {
    res.send("Mail sent")
}


module.exports = {
    purchaseItem,
    sendMail
}