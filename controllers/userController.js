const User = require('../models/userModel')
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');

const signUp = async (req, res) => {
    const hash = bcrypt.hashSync(req.body.password, saltRounds);

    const user = new User({
        name: req.body.name,
        email: req.body.email,
        password: hash
    })
    await user.save()
    res.status(201).json(user)
}

const login = async (req, res) => {
    const email = req.body.email
    const password = req.body.password

    const user = await User.findOne({ email: email }).exec();
    if (!user) {
        res.status(401).send("Unauthorized access")
    }

    const passwordMatch = bcrypt.compareSync(password, user.password);
    if (passwordMatch) {
        const token = jwt.sign({ name: user.name, email: user.email }, '69d11356fbcad445bc19f72c604f091b1a31b6717a2c561555e48061205a42068787ae89d43e99c81807d1860029542df340b5f8dd08bb845918d362a53e49da');
        res.cookie('token', token, { httpOnly: true })
        res.status(200).send("Login success")
    }
    else {
        res.status(401).send("Unauthorized access")
    }
}

module.exports = {
    signUp,
    login
}