const Category = require("../models/categoryModel");

const getAllCategories = async (req, res) => {
    const categories = await Category.find({});
    res.status(200).json(categories)
}

const addNewCategory = async (req, res) => {
    const category = new Category(req.body)
    await category.save()
    res.status(201).json(category)
}

const getCategoryById = async (req, res) => {
    const category = await Category.findById(req.params.categoryId).exec();
    res.status(200).json(category)
}

const updateCategory = async (req, res) => {
    const updatedCategory = await Category.findByIdAndUpdate(req.params.categoryId, req.body, { new: true })
    res.status(200).json(updatedCategory)
}

const deleteCategory = async (req, res) => {
    await Category.findByIdAndDelete(req.params.categoryId)
    res.send("deleted")
}

module.exports = {
    getAllCategories,
    addNewCategory,
    getCategoryById,
    updateCategory,
    deleteCategory
}