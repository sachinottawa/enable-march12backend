const express = require('express')
const { getAllProducts, addNewProduct, getProductById, updateProduct, deleteProduct } = require('../controllers/productController')
const router = express.Router()

router.get('/', getAllProducts)

router.post('/', addNewProduct)

router.get('/:productId', getProductById)

router.patch('/:productId', updateProduct)

router.delete('/:productId', deleteProduct)

module.exports = router