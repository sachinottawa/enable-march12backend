const express = require('express')
const { purchaseItem, sendMail } = require('../controllers/purchaseController')

const router = express.Router()

router.post('/send-money', checkLogin, addToStatement, sendMoney)

module.exports = router