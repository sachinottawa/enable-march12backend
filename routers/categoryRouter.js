const express = require('express')
const { getAllCategories, addNewCategory, getCategoryById, updateCategory, deleteCategory } = require('../controllers/categoryController')
const router = express.Router()

router.get('/', getAllCategories)

router.post('/', addNewCategory)

router.get('/:categoryId', getCategoryById)

router.patch('/:categoryId', updateCategory)

router.delete('/:categoryId', deleteCategory)

module.exports = router