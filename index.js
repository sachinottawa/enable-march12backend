const express = require('express')
const mongoose = require('mongoose');
const cors = require('cors')

const app = express()
const port = 3000

const productRouter = require('./routers/productRouter')
const categoryRouter = require('./routers/categoryRouter')
const userRouter = require('./routers/userRouter')

app.use(cors())
app.use(express.json())
app.use('/products', productRouter)
app.use('/categories', categoryRouter)
app.use('/users', userRouter)


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})


main().then(() => console.log("connected")).catch(err => console.log(err));

async function main() {
    await mongoose.connect('');
}